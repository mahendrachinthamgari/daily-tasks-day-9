const fs = require('fs');
const path = require('path');

/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const employees = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}


// * Store the given data into data.json

function writeAndReadData(employees) {
    fs.writeFile(path.join(__dirname, './data.json'), JSON.stringify(employees), (err) => {
        if (err) {
            console.error(err);
        } else {
            fs.readFile(path.join(__dirname, './data.json'), 'utf-8', (err, data) => {
                if (err) {
                    console.error(err);
                } else {
                    let convertedData = JSON.parse(data)
                    idsData(convertedData, [2, 13, 23], (err, data) => {
                        if (err) {
                            console.error(err);
                        } else {
                            fs.writeFile(path.join(__dirname, './idsData.json'), JSON.stringify(data), (err) => {
                                if (err) {
                                    console.error(err)
                                } else {
                                    console.log('Problem1 completed sucessfully');
                                    companiesBasedData(convertedData, (err, data) => {
                                        if (err) {
                                            console.error(err);
                                        } else {
                                            fs.writeFile(path.join(__dirname, './companyBasedData.json'), JSON.stringify(data), (err) => {
                                                if (err) {
                                                    console.error(err);
                                                } else {
                                                    console.log('problem 2 sucessfull');
                                                    powerfulBridgeData(convertedData, (err, data) => {
                                                        if (err) {
                                                            console.error(err);
                                                        } else {
                                                            fs.writeFile(path.join(__dirname, './PowerpuffBrigadeData.json'), JSON.stringify(data), (err) => {
                                                                if (err) {
                                                                    console.error(err);
                                                                } else {
                                                                    console.log('Problem 3 completed sucessfully');
                                                                    removedEntryWithId2(convertedData, (err, data) => {
                                                                        if (err) {
                                                                            console.error(err);
                                                                        } else {
                                                                            fs.writeFile(path.join(__dirname, 'removedId2.json'), JSON.stringify(data), (err) => {
                                                                                if (err) {
                                                                                    console.error(err);
                                                                                } else {
                                                                                    console.log('Problem4 completed sucessfully');
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

function idsData(employeesDetails, arrayIds = [2, 13, 23], callback) {
    const idsDetails = Object.entries(employeesDetails)[0][1].filter((emp) => {
        return arrayIds.includes(emp.id);
    })
    callback(null, idsDetails);
}

function companiesBasedData(employeesDetails, callback) {
    const companyData = Object.entries(employeesDetails)[0][1].reduce((accumulator, idDetails) => {
        accumulator[idDetails.company].push(idDetails.name);
        return accumulator;
    }, { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": [] });
    callback(null, companyData);
}

function powerfulBridgeData(employeesDetails, callback) {
    const companyData = Object.entries(employeesDetails)[0][1].filter((idDetails) => {
        return idDetails.company == 'Powerpuff Brigade';
    });
    callback(null, companyData)
}

function removedEntryWithId2(employeesDetails, callback) {
    const removedId2 = Object.entries(employeesDetails)[0][1].filter((idDetails) => {
        if (idDetails.id == 2) {
            return false;
        } else {
            return true;
        }
    });
    callback(null, removedId2);
}

function sortedCompany (employeesDetails, callback){
    const sortedCompanyData = Object.entries(employeesDetails)[0][1].sort((a, b) {
        let aCompanyName = a.company;
        let bCompanyName = b.company;
        
    })
}
writeAndReadData(employees);








